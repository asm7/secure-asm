#!/usr/bin/env bash

echo -e "\e[92mGetting GKE node IP addresses...\e[0m"
export NUM=0
for gce in $(gcloud --project ${KCC_PROJECT}-${KCC_FOLDER_ID} compute instances list --format='value(name)')
do
    export NUM=$((NUM + 1))
    export KCC_GCE_IP_$NUM=$(gcloud --project ${KCC_PROJECT}-${KCC_FOLDER_ID} compute instances describe --zone ${KCC_GKE_ZONE} $gce --format='value(networkInterfaces[0].accessConfigs[0].natIP)')
done

echo -e "\e[92mGetting Cloud Nat IP addresses...\e[0m"
export REGION_1_NAT_IP=$(kubectl --context=kcc -n ${HOST_NET_PROJECT} get computeaddress ${SUBNET_1_REGION}-static-ip -o=jsonpath='{.spec.address}')
export REGION_2_NAT_IP=$(kubectl --context=kcc -n ${HOST_NET_PROJECT} get computeaddress ${SUBNET_2_REGION}-static-ip -o=jsonpath='{.spec.address}')

echo -e "\e[92mGetting your terminal IP address...\e[0m"
export TERMINAL_IP=$(curl -s ifconfig.me)

echo -e "\e[92mUpdating GKE1...\e[0m"
gcloud --project=${SVC_1_PROJECT}-${FOLDER_ID} container clusters update ${GKE1} \
    --zone=${GKE1_ZONE} \
    --enable-master-authorized-networks \
    --master-authorized-networks $TERMINAL_IP/32,$REGION_1_NAT_IP/32,$REGION_2_NAT_IP/32,$KCC_GCE_IP_1/32,$KCC_GCE_IP_2/32,$KCC_GCE_IP_3/32,$KCC_GCE_IP_4/32 \
    --async

echo -e "\e[92mUpdating GKE2...\e[0m"
gcloud --project=${SVC_1_PROJECT}-${FOLDER_ID} container clusters update ${GKE2} \
    --zone=${GKE2_ZONE} \
    --enable-master-authorized-networks \
    --master-authorized-networks $TERMINAL_IP/32,$REGION_1_NAT_IP/32,$REGION_2_NAT_IP/32,$KCC_GCE_IP_1/32,$KCC_GCE_IP_2/32,$KCC_GCE_IP_3/32,$KCC_GCE_IP_4/32 \
    --async

echo -e "\e[92mUpdating GKE3...\e[0m"
gcloud --project=${SVC_2_PROJECT}-${FOLDER_ID} container clusters update ${GKE3} \
    --zone=${GKE3_ZONE} \
    --enable-master-authorized-networks \
    --master-authorized-networks $TERMINAL_IP/32,$REGION_1_NAT_IP/32,$REGION_2_NAT_IP/32,$KCC_GCE_IP_1/32,$KCC_GCE_IP_2/32,$KCC_GCE_IP_3/32,$KCC_GCE_IP_4/32 \
    --async

echo -e "\e[92mUpdating GKE4...\e[0m"
gcloud --project=${SVC_2_PROJECT}-${FOLDER_ID} container clusters update ${GKE4} \
    --zone=${GKE4_ZONE} \
    --enable-master-authorized-networks \
    --master-authorized-networks $TERMINAL_IP/32,$REGION_1_NAT_IP/32,$REGION_2_NAT_IP/32,$KCC_GCE_IP_1/32,$KCC_GCE_IP_2/32,$KCC_GCE_IP_3/32,$KCC_GCE_IP_4/32
