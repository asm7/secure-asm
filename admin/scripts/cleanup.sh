#!/usr/bin/env bash

# Verify that the scripts are being run from Linux and not Mac
if [[ $OSTYPE != "linux-gnu" ]]; then
    echo -e "\e[91mERROR: This script and consecutive set up scripts have only been tested on Linux. Currently, only Linux (debian) is supported. Please run in Cloud Shell or in a VM running Linux".
    exit;
fi

# Export a SCRIPT_DIR var and make all links relative to SCRIPT_DIR
export SCRIPT_DIR=$(dirname $(readlink -f $0 2>/dev/null) 2>/dev/null || echo "${PWD}/$(dirname $0)")
source ${SCRIPT_DIR}/../scripts/functions.sh

# Set speed
bold=$(tput bold)
normal=$(tput sgr0)

color='\e[1;32m' # green
nc='\e[0m'

echo -e "\n"
title_no_wait "*** CLEANUP ***"
echo -e "\n"

if [[ ! ${WORKDIR} ]]; then
    title_no_wait "You have not defined your WORKDIR variable."
    exit 1
fi

print_and_execute "source $WORKDIR/vars.sh"

if gcloud projects list --filter "${KCC_PROJECT}-${KCC_FOLDER_ID}" | grep "${KCC_PROJECT}-${KCC_FOLDER_ID}"; then
    print_and_execute "export FOLDER_ID=$(kubectl --context=${KCC_GKE} -n ${ORG_SHORT_NAME} get folder -o=jsonpath='{.items[].status.folderId}')"
    [ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" && exit 1 || echo "FOLDER_ID is $FOLDER_ID"
    echo -e "export FOLDER_ID=$FOLDER_ID" >> $WORKDIR/vars.sh
fi

echo -e "\n"
title_no_wait "Deleting host net project lien..."
print_and_execute "export LIEN_ID=$(gcloud alpha resource-manager liens list --project="${HOST_NET_PROJECT}-${FOLDER_ID}" | awk 'NR==2 {print $1}')"
[ -z "$LIEN_ID" ] && echo "LIEN_ID not found" || print_and_execute "gcloud alpha resource-manager liens delete $LIEN_ID"

echo -e "\n"
title_no_wait "Deleting svc1 project..."
if gcloud projects list --filter "${SVC_1_PROJECT}-${FOLDER_ID}" | grep "${SVC_1_PROJECT}-${FOLDER_ID}"; then
  print_and_execute "gcloud projects delete "${SVC_1_PROJECT}-${FOLDER_ID}" --quiet"
fi

echo -e "\n"
title_no_wait "Deleting svc2 project..."
if gcloud projects list --filter "${SVC_2_PROJECT}-${FOLDER_ID}" | grep "${SVC_2_PROJECT}-${FOLDER_ID}"; then
  print_and_execute "gcloud projects delete "${SVC_2_PROJECT}-${FOLDER_ID}" --quiet"
fi

echo -e "\n"
title_no_wait "Deleting host net project..."
if gcloud projects list --filter "${HOST_NET_PROJECT}-${FOLDER_ID}" | grep "${HOST_NET_PROJECT}-${FOLDER_ID}"; then
  print_and_execute "gcloud projects delete "${HOST_NET_PROJECT}-${FOLDER_ID}" --quiet"
fi

echo -e "\n"
title_no_wait "Deleting folder..."
if gcloud resource-manager folders list --organization="$ORG_ID" --filter "$FOLDER_ID" | grep "$FOLDER_ID"; then
  print_and_execute "gcloud resource-manager folders delete ${FOLDER_ID}"
fi

echo -e "\n"
title_no_wait "Deleting KCC GCP service account organization level IAM roles..."
export KCC_SA_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts list | grep ${KCC_SERVICE_ACCOUNT})
if [ "$KCC_SA_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.organizationAdmin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/billing.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.folderAdmin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/resourcemanager.projectCreator'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/compute.admin'
fi

echo -e "\n"
title_no_wait "Deleting Gitlab CI GCP service account organization level IAM roles..."
export GITLAB_SA_EXISTS=$(gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts list | grep ${GITLAB_CI_GCP_SA_NAME})
if [ "$GITLAB_SA_EXISTS" ]; then
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/gkehub.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/compute.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/container.admin'

    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
    --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --role='roles/owner'
fi

echo -e "\n"
title_no_wait "Deleting kcc project..."
if gcloud projects list --filter "${KCC_PROJECT}-${KCC_FOLDER_ID}" | grep "${KCC_PROJECT}-${KCC_FOLDER_ID}"; then
  print_and_execute "gcloud projects delete "${KCC_PROJECT}-${KCC_FOLDER_ID}" --quiet"
fi

echo -e "\n"
title_no_wait "Deleting kcc folder..."
if gcloud resource-manager folders list --organization="$ORG_ID" --filter "$KCC_FOLDER_ID" | grep "$KCC_FOLDER_ID"; then
  print_and_execute "gcloud resource-manager folders delete ${KCC_FOLDER_ID}"
fi

echo -e "\n"
title_no_wait "Deleting Online Boutique Gitlab project..."
export GITLAB_SHOP_PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_SHOP_PROJECT_NAME "$GITLAB_SHOP_PROJECT_NAME" '.[] | select(.name==$GITLAB_SHOP_PROJECT_NAME)' | jq -r '.id')
if [ $GITLAB_SHOP_PROJECT_ID ]; then
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/projects/$GITLAB_SHOP_PROJECT_ID"
fi

echo -e "\n"
title_no_wait "Deleting Bank of Anthos Gitlab project..."
export GITLAB_BANK_PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_BANK_PROJECT_NAME "$GITLAB_BANK_PROJECT_NAME" '.[] | select(.name==$GITLAB_BANK_PROJECT_NAME)' | jq -r '.id')
if [ $GITLAB_BANK_PROJECT_ID ]; then
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/projects/$GITLAB_BANK_PROJECT_ID"
fi

echo -e "\n"
title_no_wait "Deleting Infrastructure Gitlab project..."
export GITLAB_PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_PROJECT_NAME "$GITLAB_PROJECT_NAME" '.[] | select(.name==$GITLAB_PROJECT_NAME)' | jq -r '.id')
if [ $GITLAB_PROJECT_ID ]; then
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID"
fi

echo -e "\n"
title_no_wait "Deleting Gitlab SSH key..."
export GITLAB_SSH_KEY_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/keys | jq --arg GITLAB_PROJECT_NAME "$GITLAB_PROJECT_NAME" '.[] | select(.title=="asm-shared-vpc-key")' | jq -r '.id')
if [ $GITLAB_SSH_KEY_ID ]; then
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/user/keys/$GITLAB_SSH_KEY_ID"
fi

echo -e "\n"
title_no_wait "Deleting KCC GCP service account as a billing user..."
gcloud beta billing accounts get-iam-policy ${BILLING_ACCOUNT} --format=json | \
jq '(.bindings[] | select(.role=="roles/billing.user").members) -= ["serviceAccount:'${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com'"]' > $WORKDIR/billing-iam-policy.json
gcloud beta billing accounts set-iam-policy ${BILLING_ACCOUNT} $WORKDIR/billing-iam-policy.json

echo -e "\n"
title_no_wait "Unsetting KUBECONFIG..."
unset KUBECONFIG