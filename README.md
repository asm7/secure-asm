# Secure ASM

<img src="admin/docs/arch.svg" width=90% height=90%>

## Setting up the environment

1.  Create a `WORKDIR` for this guide. At the end you can delete all the resources and this folder.

    ```bash
    mkdir -p asm-secure-gitlab && cd asm-secure-gitlab && export WORKDIR=`pwd`
    ```

1.  Create environment variables used in this guide. You use environment variables throughout this guide. It is best to save them in a `vars.sh` file, in case you lose access to your terminal (and your envars), you can source the vars.sh.

    ```bash
    export ADMIN_USER=[Enter Org admin email for example you@org.xyz]
    export ORG_NAME=[Your Organization name for example org.xyz]
    export BILLING_ACCOUNT=[Enter Billing Account ID for example C0FFEE-123456-AABBCC]
    export ORG_ID=[Enter Org ID for example 1234567890 from gcloud organizations list]
    ```

1.  Copy the remainder of the variables in your `vars.sh` file.

    ```bash
    export ORG_SHORT_NAME=${ORG_NAME%.*}
    export USER_NAME=${ADMIN_USER%@*}
    export USER_NAME=${USER_NAME:0:7}
    export SUBNET_1_REGION=us-west2
    export SUBNET_2_REGION=us-central1
    export BUILD_NUMBER=01
    cat <<EOF >> $WORKDIR/vars.sh
    # Copy and paste the remainder of the variables
    # Org
    export WORKDIR=$WORKDIR
    export ADMIN_USER=$ADMIN_USER
    export ORG_NAME=$ORG_NAME
    export BILLING_ACCOUNT=$BILLING_ACCOUNT
    export ORG_ID=$ORG_ID
    export ORG_SHORT_NAME=${ORG_SHORT_NAME}
    export USER_NAME=${USER_NAME}
    export BUILD_NUMBER=${BUILD_NUMBER}
    export MAIN_FOLDER_NAME=${ORG_SHORT_NAME}-${USER_NAME}-${BUILD_NUMBER}-asm
    # KCC
    export KCC_FOLDER_NAME=${USER_NAME}-asm-secure-kcc
    export KCC_PROJECT=proj-infra-admin
    export KCC_GKE=kcc
    export KCC_GKE_ZONE=us-central1-f
    export KCC_SERVICE_ACCOUNT=kcc-sa
    # GCP Infra
    export FOLDER_NAME=${USER_NAME}-asm-secure-infra
    export HOST_NET_PROJECT=proj-0-net-prod
    export SVC_1_PROJECT=proj-1-bank-prod
    export SVC_2_PROJECT=proj-2-shop-prod
    export SVC_3_PROJECT=proj-3-pci-prod
    # Gitlab groups and projects
    export GITLAB_CI_GCP_SA_NAME=gitlab-ci-sa
    export GITLAB_GROUP_NAME=${ORG_SHORT_NAME}-${USER_NAME}-${BUILD_NUMBER}-asm
    export GITLAB_INFRA_PROJECT_NAME=infrastructure
    export GITLAB_BANK_PROJECT_NAME=bank
    export GITLAB_SHOP_PROJECT_NAME=shop
    export GITLAB_CONFIG_PROJECT_NAME=config
    export GITLAB_REDIS_PROJECT_NAME=redis
    export GITLAB_COCKROACHDB_PROJECT_NAME=cockroachdb
    export GITLAB_SSH_KEY=${ORG_SHORT_NAME}-${USER_NAME}-${BUILD_NUMBER}-asm-sshkey
    # GKE clusters and zones
    export GKE1=gke-1-r1a-prod
    export GKE2=gke-2-r1b-prod
    export GKE3=gke-3-r2a-prod
    export GKE4=gke-4-r2b-prod
    export GKE5=gke-5-r1a-prod
    export GKE6=gke-6-r1b-prod
    export GKE7=gke-7-r2a-prod
    export GKE8=gke-8-r2b-prod
    export GKE9=gke-9-r1c-prod
    export GKE10=gke-10-r2c-prod
    export GKE1_ZONE=${SUBNET_1_REGION}-a
    export GKE2_ZONE=${SUBNET_1_REGION}-b
    export GKE3_ZONE=${SUBNET_2_REGION}-a
    export GKE4_ZONE=${SUBNET_2_REGION}-b
    export GKE5_ZONE=${SUBNET_1_REGION}-a
    export GKE6_ZONE=${SUBNET_1_REGION}-b
    export GKE7_ZONE=${SUBNET_2_REGION}-a
    export GKE8_ZONE=${SUBNET_2_REGION}-b
    export GKE9_ZONE=${SUBNET_1_REGION}-c
    export GKE10_ZONE=${SUBNET_2_REGION}-c
    export KUBECONFIG=${WORKDIR}/asm-kubeconfig
    # ASM
    export ASM_MAJOR_VERSION=1.8
    export ASM_VERSION=1.8.3-asm.2
    export ASM_LABEL=asm-183-2
    # My IP
    export TERMINAL_IP=$(curl -s ifconfig.me)
    EOF
    ```

1.  Source vars.sh. In case you lose your terminal, you must always source vars.sh before proceeding.

    ```bash
    source $WORKDIR/vars.sh
    ```

1.  Install [krew](https://krew.sigs.k8s.io). Krew is a kubectl plugin manager that helps you discover and manage kubectl plugins. Two of these plugins are ctx and ns which make switching between multiple GKE clusters easier.

    ```bash
    (
    set -x; cd "$(mktemp -d)" &&
    curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
    tar zxvf krew.tar.gz &&
    KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
    $KREW install --manifest=krew.yaml --archive=krew.tar.gz &&
    $KREW update
    )

    echo -e "export PATH="${PATH}:${HOME}/.krew/bin"" >> ~/.bashrc && source ~/.bashrc # .zshrc if using ZSH
    ```

1.  Install `ctx` and `ns` via krew for easy context switching.

    ```bash
    kubectl krew install ctx
    kubectl krew install ns
    kubectl krew install get-all
    ```

## Creating folder, project and GKE cluster for KCC

In this section, you create a folder and a project for KCC. This is a project owned by infrastructure administrators used to create GCP resources for example folders, projects, networks, IAM permissions and GKE clusters.

1.  Update `gcloud` and install alpha and beta components.

    ```bash
    gcloud components install alpha beta
    gcloud components update
    ```

In Cloud Shell, you may have to use `sudo apt-get install` to install gcloud components. Follow the instructions in Cloud Shell. This step may take a few minutes to complete.

1.  Login with your admin user account.

    ```bash
    gcloud config set account ${ADMIN_USER}
    gcloud auth login
    gcloud config unset project
    ```

1.  Create a main folder. Inside the main folder are two sub-folders. One sub-folder contains GCP builder project. The other sub-folder is where the infrastructure resources are created. Creating folders allows you to easily keep track of resources required for this guide in one place. At the end, you can delete all resources in the main folder.

    ```bash
    # Create the main folder
    gcloud alpha resource-manager folders create \
    --display-name=${MAIN_FOLDER_NAME} \
    --organization=${ORG_ID}

    # Get Main folder IP
    export MAIN_FOLDER_ID=$(gcloud resource-manager folders list --organization=${ORG_ID} | grep ${MAIN_FOLDER_NAME} | awk '{print $3}')
    [ -z "$MAIN_FOLDER_ID" ] && echo "MAIN_FOLDER_ID is not exported" || echo "MAIN_FOLDER_ID is $MAIN_FOLDER_ID"
    echo -e "export MAIN_FOLDER_ID=$MAIN_FOLDER_ID" >> $WORKDIR/vars.sh
    source ${WORKDIR}/vars.sh

    # Create the KCC builder folder inside the main folder
    gcloud alpha resource-manager folders create \
    --display-name=${KCC_FOLDER_NAME} \
    --folder=${MAIN_FOLDER_ID}

    # Get KCC folder IP
    export KCC_FOLDER_ID=$(gcloud resource-manager folders list --folder=${MAIN_FOLDER_ID} | grep ${KCC_FOLDER_NAME} | awk '{print $3}')
    [ -z "$KCC_FOLDER_ID" ] && echo "KCC_FOLDER_ID is not exported" || echo "KCC Folder ID is $KCC_FOLDER_ID"
    echo -e "export KCC_FOLDER_ID=$KCC_FOLDER_ID" >> $WORKDIR/vars.sh
    ```

1.  Create a project for KCC.

    ```bash
    gcloud projects create ${KCC_PROJECT}-${KCC_FOLDER_ID} \
      --folder ${KCC_FOLDER_ID}
    gcloud beta billing projects link ${KCC_PROJECT}-${KCC_FOLDER_ID} \
      --billing-account ${BILLING_ACCOUNT}

    gcloud services enable \
      --project ${KCC_PROJECT}-${KCC_FOLDER_ID} \
      container.googleapis.com \
      cloudbilling.googleapis.com \
      cloudbuild.googleapis.com \
      sqladmin.googleapis.com \
      servicenetworking.googleapis.com \
      cloudresourcemanager.googleapis.com

    gcloud container clusters create ${KCC_GKE} \
      --project=${KCC_PROJECT}-${KCC_FOLDER_ID} \
      --zone=${KCC_GKE_ZONE} \
      --machine-type "e2-standard-4" \
      --num-nodes "2" --min-nodes "2" --max-nodes "4" \
      --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
      --release-channel regular \
      --addons ConfigConnector \
      --workload-pool=${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog

    touch ${WORKDIR}/asm-kubeconfig && export KUBECONFIG=${WORKDIR}/asm-kubeconfig
    echo -e "export KUBECONFIG=$WORKDIR/asm-kubeconfig" >> $WORKDIR/vars.sh
    gcloud container clusters get-credentials ${KCC_GKE} --zone ${KCC_GKE_ZONE} --project ${KCC_PROJECT}-${KCC_FOLDER_ID}

    kubectl ctx ${KCC_GKE}=gke_${KCC_PROJECT}-${KCC_FOLDER_ID}_${KCC_GKE_ZONE}_${KCC_GKE}
    ```

## Creating an identity for KCC

In this section, you configure identity for KCC to create GCP resources. Config Connector creates and manages Google Cloud resources by authenticating with a Identity and Access Management (IAM) service account and using GKE's Workload Identity to bind IAM service accounts with Kubernetes service accounts.

1.  Create a service account and assign Organization Admin, Folder Admin, Project Creator, Billing Account Admin and Compute Admin roles to the account.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${KCC_SERVICE_ACCOUNT}

    project_roles=(
        roles/resourcemanager.organizationAdmin
        roles/billing.admin
        roles/resourcemanager.folderAdmin
        roles/resourcemanager.projectCreator
        roles/compute.admin
        )
    for role in "${project_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
          --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
          --role="${role}"
    done
    ```

1.  Give the KCC SA billing user permission to the billing account ID.

    ```bash
    gcloud beta billing accounts get-iam-policy ${BILLING_ACCOUNT} --format=json | \
    jq '(.bindings[] | select(.role=="roles/billing.user").members) += ["serviceAccount:'${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com'"]' > kcc-sa-billing-iam-policy.json
    gcloud beta billing accounts set-iam-policy ${BILLING_ACCOUNT} kcc-sa-billing-iam-policy.json
    ```

1.  Create an IAM policy binding between the KCC GCP service account and the predefined Kubernetes service account that Config Connector runs.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts add-iam-policy-binding \
    ${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
    --member="serviceAccount:${KCC_PROJECT}-${KCC_FOLDER_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager]" \
    --role="roles/iam.workloadIdentityUser"
    ```

The output is similar to the following:

    Updated IAM policy for serviceAccount [kcc-sa@proj-infra-admin-389773461808.iam.gserviceaccount.com].
    bindings:
    - members:
      - serviceAccount:proj-infra-admin-389773461808.svc.id.goog[cnrm-system/cnrm-controller-manager]
      role: roles/iam.workloadIdentityUser
    etag: BwW6ev3b5kc=
    version: 1

1.  Create the config connector resource.

    ```bash
    cat <<EOF > ${KCC_GKE}-configconnector.yaml
    apiVersion: core.cnrm.cloud.google.com/v1beta1
    kind: ConfigConnector
    metadata:
      # the name is restricted to ensure that there is only one
      # ConfigConnector instance installed in your cluster
      name: configconnector.core.cnrm.cloud.google.com
    spec:
      mode: cluster
      googleServiceAccount: "${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com"
    EOF

    kubectl --context=${KCC_GKE} apply -f ${KCC_GKE}-configconnector.yaml
    ```

The output is similar to the following:

    configconnector.core.cnrm.cloud.google.com/configconnector.core.cnrm.cloud.google.com configured

## Creating an organization namespace

In this section, you create a namespace for organization. All organization level resources like folders or projects are created in the organization namespace.

1.  Create an org namespace.

    ```bash
    cat <<EOF > ${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml
    apiVersion: v1
    kind: Namespace
    metadata:
      name: ${ORG_SHORT_NAME}
      annotations:
        cnrm.cloud.google.com/organization-id: "${ORG_ID}"
    EOF

    kubectl --context=${KCC_GKE} apply -f ${KCC_GKE}-${ORG_SHORT_NAME}-namespace.yaml
    ```

## Creating service account for Gitlab CI

In this section, you create a GCP Service Account in the `proj-infra-admin` project that is used by the Gitlab CI to deploy infrastructure.

1.  Create a GCP service for Gitlab CI. Assign the appropriate IAM roles that the Gitlab CI needs to deploy resources to GCP.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} iam service-accounts create ${GITLAB_CI_GCP_SA_NAME}

    project_roles=(
        roles/gkehub.admin
        roles/compute.admin
        roles/container.admin
        roles/owner
        )
    for role in "${project_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations add-iam-policy-binding ${ORG_ID} \
          --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
          --role="${role}"
    done
    ```

1.  Download the Gitlab GCP SA credentials and create a base64 version for Gitlab CI.

    ```bash
    gcloud iam service-accounts keys create ${GITLAB_CI_GCP_SA_NAME}-key.json \
      --iam-account ${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com
    cat ${GITLAB_CI_GCP_SA_NAME}-key.json | base64 > ${GITLAB_CI_GCP_SA_NAME}-key-base64.txt
    ```

## Creating the builder image for Gitlab CI

In this section, you create a Docker image with all the tools required to deploy resources for this guide.

1.  Clone the ASM repo.

    ```bash
    git clone https://gitlab.com/asm7/secure-asm ${WORKDIR}/asm-secure-in-workdir
    cd ${WORKDIR}/asm-secure-in-workdir/infrastructure/builder
    ```

1.  Create a `builder` Docker image which is used by Gitlab CI. This image contains all the tools required to deploy resources to GCP. These tools include gcloud, kubectl, and common bash utilities. Push the image to the GCR repo in the `proj-infra-admin` project.

    ```bash
    gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} builds submit . --tag=gcr.io/${KCC_PROJECT}-${KCC_FOLDER_ID}/builder
    ```

The output is similar to the following:

    ...
    33e1791e-f1a4-4c50-98d0-ff63338d3a4b  2021-02-09T19:11:13+00:00  2M22S     gs://proj-infra-admin-1053315125805_cloudbuild/source/1612897872.028883-1ae85c6643044bc4b247da6dff4d4412.tgz  gcr.io/proj-infra-admin-1053315125805/builder (+1 more)  SUCCESS

1.  Make the `builder` Docker image public so that Gitlab CI can use the image.

    ```bash
    gsutil defacl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -r -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    gsutil acl ch -u AllUsers:R gs://artifacts.${KCC_PROJECT}-${KCC_FOLDER_ID}.appspot.com
    cd ${WORKDIR}
    ```

## Creating Gitlab group and projects

In this section, you create a Gitlab group and an `infra` project and add the `kcc` cluster to the project so that Gitlab CI can use runners in the `kcc` cluster.

### Preparing Gitlab credentials

1.  Create an access token to Gitlab so that you can use Gitlab APIs to create and configure projects. Access the following link. Name the token and check **api** under **Scopes**. Click **Create personal access token**.

    ```bash
    echo -n "https://gitlab.com/-/profile/personal_access_tokens"
    ```

<img src="admin/docs/gitlab-api-token.png" width=90% height=90%>

Copy the personal access token to a notepad or Google Keep app. It is important not to share this token with anyone since it provides complete read/write API access to your Gitlab account. You can revoke the token after you're done.

1.  Create a variable with your Gitlab personal access token.

    ```bash
    export GITLAB_TOKEN=<Gitlab personal access token>
    echo -e "export GITLAB_TOKEN=$GITLAB_TOKEN" >> $WORKDIR/vars.sh
    ```

### Creating Gitlab group and configure shared Gitlab runners

1.  Create an SSH keypair to use with Gitlab to clone and update repos.

    ```bash
    mkdir -p ${WORKDIR}/tmp && cd ${WORKDIR}/tmp
    ssh-keygen -t rsa -b 4096 \
    -C "${ADMIN_USER}" \
    -N '' \
    -f ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key
    eval `ssh-agent`
    ssh-add ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key
    echo -e "eval \`ssh-agent\` && ssh-add ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key" >> $WORKDIR/vars.sh
    ```

1.  Add the SSH public key to your Gitlab account.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/user/keys" --form "title=${GITLAB_SSH_KEY}" --form "key=$(cat ${WORKDIR}/tmp/${GITLAB_SSH_KEY}-key.pub)"
    ```

1.  Get your Gitlab user ID.

    ```bash
    export GITLAB_USER_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/user | jq -r '.id')
    [ -z "$GITLAB_USER_ID" ] && echo "GITLAB_USER_ID is not exported" || echo "GITLAB_USER_ID is $GITLAB_USER_ID"
    echo -e "export GITLAB_USER_ID=$GITLAB_USER_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_USER_ID is 12345679

1.  Create a new Gitlab group using the Gitlab API and get the group ID.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.com/api/v4/groups?name=${GITLAB_GROUP_NAME}&path=${GITLAB_GROUP_NAME}"

    # Get group ID
    export GITLAB_GROUP_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups" | jq --arg GITLAB_GROUP_NAME "$GITLAB_GROUP_NAME" '.[] | select(.name==$GITLAB_GROUP_NAME)' | jq -r '.id')
    [ -z "$GITLAB_GROUP_ID" ] && echo "GITLAB_GROUP_ID is not exported" || echo "GITLAB_GROUP_ID is $GITLAB_GROUP_ID"
    echo -e "export GITLAB_GROUP_ID=$GITLAB_GROUP_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_GROUP_ID is 123456789

1.  Get runner's registration code from the Gitlab group. You can use this code to deploy your Gitlab runners in the `kcc` cluster.

    ```bash
    export GITLAB_RUNNER_TOKEN=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID" | jq -r '.runners_token')
    [ -z "$GITLAB_RUNNER_TOKEN" ] && echo "GITLAB_RUNNER_TOKEN is not exported" || echo "GITLAB_RUNNER_TOKEN is $GITLAB_RUNNER_TOKEN"
    echo -e "export GITLAB_RUNNER_TOKEN=$GITLAB_RUNNER_TOKEN" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_RUNNER_TOKEN is abCDef1223DCba

Use helm v3 to deploy the runner in the `kcc` cluster. Otherwise, [install helm](https://helm.sh/docs/intro/install/).

1.  Verify you have helm v3 installed in your terminal.

    ```bash
    helm version
    ```

The output is similar to the following:

    version.BuildInfo{Version:"v3.5.0"...}

1.  Deploy the helm chart.

    ```bash
    helm repo add gitlab https://charts.gitlab.io

    kubectl --context=${KCC_GKE} create namespace gitlab

    helm install --namespace gitlab gitlab-runner --set gitlabUrl="https://gitlab.com" \
    --set runnerRegistrationToken="$GITLAB_RUNNER_TOKEN" \
    --set rbac.create=true \
    --set rbac.clusterWideAccess=true gitlab/gitlab-runner
    ```

The output is similar to the following:

    "NAME: gitlab-runner
    LAST DEPLOYED: Mon Feb  8 23:37:48 2021
    NAMESPACE: gitlab
    STATUS: deployed
    REVISION: 1
    TEST SUITE: None
    NOTES:
    Your GitLab Runner should now be registered against the GitLab instance reachable at: ""https://www.gitlab.com"""

1.  Wait until the Gitlab runner is Ready.

    ```bash
    kubectl --context=${KCC_GKE} -n gitlab wait --for=condition=available deployment gitlab-runner-gitlab-runner --timeout=5m
    ```

The output is similar to the following:

    deployment.apps/gitlab-runner-gitlab-runner condition met

1.  List the runner in the group. You see the runner you just deployed.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID/runners?status=active" | jq '.[] | select(.is_shared==false)'
    ```

The output is similar to the following:

    {
      "id": 123456,
      "description": "gitlab-runner-gitlab-runner-123456abcde-pjrqc",
      "ip_address": "104.104.140.140",
      "active": true,
      "is_shared": false,
      "name": "gitlab-runner",
      "online": true,
      "status": "online"
    }

1.  Create variables in your Gitlab groups which can be used by all Gitlab projects inside this group. These variables are used for Gitlab CI pipeline.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_GCP_SA_KEY" --form "value=$(cat ${WORKDIR}/${GITLAB_CI_GCP_SA_NAME}-key-base64.txt)" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=BILLING_ACCOUNT" --form "value=$BILLING_ACCOUNT" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=GITLAB_GCP_SA" --form "value=${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=INFRA_ADMIN_PROJECT_ID" --form "value=${KCC_PROJECT}-${KCC_FOLDER_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=MAIN_FOLDER_ID" --form "value=${MAIN_FOLDER_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ORG_ID" --form "value=${ORG_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ORG_NAME" --form "value=${ORG_SHORT_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=FOLDER_NAME" --form "value=${FOLDER_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=TERMINAL_IP" --form "value=${TERMINAL_IP}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ASM_MAJOR_VERSION" --form "value=${ASM_MAJOR_VERSION}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ASM_VERSION" --form "value=${ASM_VERSION}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/groups/${GITLAB_GROUP_ID}/variables" --form "key=ASM_LABEL" --form "value=${ASM_LABEL}" --form "protected=true"
    ```

1.  Verify the variables are properly configured.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/groups/"${GITLAB_GROUP_ID}"/variables | jq
    ```

### Creating Gitlab projects

In this section, you create four Gitlab projects inside the group.

- _infra_ - This repo contains [Iac]() to build GCP resources for example projects, networks, GKE clusters etc.
- _config_ - This is the [ACM]() repo used for deploying applications and policies to the GKE clusters.
- _bank_ - This is a demo application repo containing the source code and service manifests (Kubernetes manifests) for the Bank of Anthos application.
- _shop_ - This is a demo application repo containing the source code and service manifests (Kubernetes manifests) for the Online Boutique application.

1.  Create four Gitlab projects (or repos) inside the group created previously.

    ```bash
    gitlab_project=(
        ${GITLAB_INFRA_PROJECT_NAME}
        ${GITLAB_CONFIG_PROJECT_NAME}
        ${GITLAB_BANK_PROJECT_NAME}
        ${GITLAB_SHOP_PROJECT_NAME}
    )
    for project in "${gitlab_project[@]}"
    do
        echo -e "\e[95mCreating $project Gitlab project...\n\e[0m"
        curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=${project}&namespace_id=${GITLAB_GROUP_ID}&shared_runners_enabled=false"
        export GITLAB_PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_GROUP_NAME%2F$project" | jq -r '.id')
        echo -e "\e[95m$project project ID is $GITLAB_PROJECT_ID.\n\e[0m"
        echo -e "\e[95mSetting default branch to main.\n\e[0m"
        curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -H "Content-Type: application/json" -X POST "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/repository/branches?branch=main&ref=master"
        curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -H "Content-Type: application/json" -X PUT "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}?default_branch=main"
        curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -H "Content-Type: application/json" -X DELETE "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/repository/branches/master"
        curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -H "Content-Type: application/json" -X POST "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/protected_branches?name=main"
        if [ $project = "infrastructure" ]; then
            echo -e "export GITLAB_INFRA_PROJECT_ID=${GITLAB_PROJECT_ID}" >> $WORKDIR/vars.sh
            export GITLAB_INFRA_PROJECT_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}" | jq -r '.ssh_url_to_repo')
            [ -z "$GITLAB_INFRA_PROJECT_SSH_URL" ] && echo "GITLAB_INFRA_PROJECT_SSH_URL is not exported" || echo "GITLAB_INFRA_PROJECT_SSH_URL is $GITLAB_INFRA_PROJECT_SSH_URL"
            echo -e "export GITLAB_INFRA_PROJECT_SSH_URL=$GITLAB_INFRA_PROJECT_SSH_URL" >> $WORKDIR/vars.sh
            export GITLAB_INFRA_PROJECT_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}" | jq -r '.web_url')
            [ -z "$GITLAB_INFRA_PROJECT_WEB_URL" ] && echo "GITLAB_INFRA_PROJECT_WEB_URL is not exported" || echo "GITLAB_INFRA_PROJECT_WEB_URL is $GITLAB_INFRA_PROJECT_WEB_URL"
            echo -e "export GITLAB_INFRA_PROJECT_WEB_URL=$GITLAB_INFRA_PROJECT_WEB_URL" >> $WORKDIR/vars.sh
        fi
        if [ $project = "config" ]; then
            echo -e "export GITLAB_CONFIG_PROJECT_ID=${GITLAB_PROJECT_ID}" >> $WORKDIR/vars.sh
            export GITLAB_CONFIG_PROJECT_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}" | jq -r '.ssh_url_to_repo')
            [ -z "$GITLAB_CONFIG_PROJECT_SSH_URL" ] && echo "GITLAB_CONFIG_PROJECT_SSH_URL is not exported" || echo "GITLAB_CONFIG_PROJECT_SSH_URL is $GITLAB_CONFIG_PROJECT_SSH_URL"
            echo -e "export GITLAB_CONFIG_PROJECT_SSH_URL=$GITLAB_CONFIG_PROJECT_SSH_URL" >> $WORKDIR/vars.sh
            export GITLAB_CONFIG_PROJECT_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}" | jq -r '.web_url')
            [ -z "$GITLAB_CONFIG_PROJECT_WEB_URL" ] && echo "GITLAB_CONFIG_PROJECT_WEB_URL is not exported" || echo "GITLAB_CONFIG_PROJECT_WEB_URL is $GITLAB_CONFIG_PROJECT_WEB_URL"
            echo -e "export GITLAB_CONFIG_PROJECT_WEB_URL=$GITLAB_CONFIG_PROJECT_WEB_URL" >> $WORKDIR/vars.sh
        fi
        if [ $project = "bank" ]; then
            echo -e "export GITLAB_BANK_PROJECT_ID=${GITLAB_PROJECT_ID}" >> $WORKDIR/vars.sh
            export GITLAB_BANK_PROJECT_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}" | jq -r '.ssh_url_to_repo')
            [ -z "$GITLAB_BANK_PROJECT_SSH_URL" ] && echo "GITLAB_BANK_PROJECT_SSH_URL is not exported" || echo "GITLAB_BANK_PROJECT_SSH_URL is $GITLAB_BANK_PROJECT_SSH_URL"
            echo -e "export GITLAB_BANK_PROJECT_SSH_URL=$GITLAB_BANK_PROJECT_SSH_URL" >> $WORKDIR/vars.sh
            export GITLAB_BANK_PROJECT_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}" | jq -r '.web_url')
            [ -z "$GITLAB_BANK_PROJECT_WEB_URL" ] && echo "GITLAB_BANK_PROJECT_WEB_URL is not exported" || echo "GITLAB_BANK_PROJECT_WEB_URL is $GITLAB_BANK_PROJECT_WEB_URL"
            echo -e "export GITLAB_BANK_PROJECT_WEB_URL=$GITLAB_BANK_PROJECT_WEB_URL" >> $WORKDIR/vars.sh
        fi
        if [ $project = "shop" ]; then
            echo -e "export GITLAB_SHOP_PROJECT_ID=${GITLAB_PROJECT_ID}" >> $WORKDIR/vars.sh
            export GITLAB_SHOP_PROJECT_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}" | jq -r '.ssh_url_to_repo')
            [ -z "$GITLAB_SHOP_PROJECT_SSH_URL" ] && echo "GITLAB_SHOP_PROJECT_SSH_URL is not exported" || echo "GITLAB_SHOP_PROJECT_SSH_URL is $GITLAB_SHOP_PROJECT_SSH_URL"
            echo -e "export GITLAB_SHOP_PROJECT_SSH_URL=$GITLAB_SHOP_PROJECT_SSH_URL" >> $WORKDIR/vars.sh
            export GITLAB_SHOP_PROJECT_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}" | jq -r '.web_url')
            [ -z "$GITLAB_SHOP_PROJECT_WEB_URL" ] && echo "GITLAB_SHOP_PROJECT_WEB_URL is not exported" || echo "GITLAB_SHOP_PROJECT_WEB_URL is $GITLAB_SHOP_PROJECT_WEB_URL"
            echo -e "export GITLAB_SHOP_PROJECT_WEB_URL=$GITLAB_SHOP_PROJECT_WEB_URL" >> $WORKDIR/vars.sh
        fi
    done
    source ${WORKDIR}/vars.sh
    ```

## Deploying Infrastructure

1.  Clone the `infrastructure` repo using SSH.

    ```bash
    cd ${WORKDIR}
    git clone $GITLAB_INFRA_PROJECT_SSH_URL ${WORKDIR}/${ORG_SHORT_NAME}-asm-secure-infra
    ```

1.  Copy the contents of the Secure ASM repo into your `infrastructure` repo.

    ```bash
    cp -r ${WORKDIR}/asm-secure-in-workdir/infrastructure/. ${WORKDIR}/${ORG_SHORT_NAME}-asm-secure-infra
    ```

1.  Commit the changes to your repo.

    ```bash
    cd ${WORKDIR}/${ORG_SHORT_NAME}-asm-secure-infra
    git add .
    git commit -m "initial commit"
    git push
    ```

## Inspecting pipelines

In this section, you inspect the Gitlab pipelines which deploy resources in your GCP organization.

1.  Access the following link to inspect the Gitlab pipeline.

    ```bash
    echo -n "${GITLAB_INFRA_PROJECT_WEB_URL}/-/pipelines"
    ```

<img src="/admin/docs/infra-pipeline.png" width=90% height=90%>

You can click on individual stages and monitor progress of all the steps within each stage.  
This pipeline takes about 25 to 30 minutes to complete.

## Pipeline Stages

This section briefly explains what each stage in the pipeline accomplishes.

### Folder

This stage creates a folder where all of the projects are created. Creating a folder makes it easier to organize resources for this guide.

### Projects

This stage creates four projects.

- One project is the host network project owned by the network admin team where they create a shared VPC and all networking resources for the organization.
- Two projects are service projects belonging to the application teams -- `bank` and `shop`. Each application team gets their own GCP project; however, both projects use the shared VPC for network resources.
- The fourth project is the PCI and data project. This project contains clusters for PCI applications. The clusters also houses databases like Redis and CockroachDB.

### Network

This stage creates a VPC network in the host network project. It also creates two subnets and secondary ranges required for the GKE clusters. The two subnets created are in two different regions.

### SharedVPC

This stage configures the shared VPC. Host network project is configured as the host project and the other two projects (bank and shop) are added as service projects. Each service project is allowed one subnet that they can use for resources (for example GKE clusters) in that project.

### CloudNAT

This stage creates Cloud Nat gateways in the two regions. Cloud Nat gateways are used by private GKE clusters so that the Pods running inside GKE clusters can access the internet. This is also required by ASM, because the ASM controlplane Pods need access to all the GKE clusters' API servers. ASM controlplane Pods use Cloud Nat gateways in their region to access GKE API servers.

### GKE

This stage creates four GKE clusters. Two clusters each are created in the `bank` and `shop` projects. The clusters are configured as private clusters. This means that the nodes do not get a public IP address. The clusters are also configured with `master-authorized-networks` enabled. This means that only specific CIDR IP ranges are allowed access to the GKE API servers. GKE clusters are configured with both region Cloud Nat gateway public IP addresses as well as the Gitlab CI runner public IP.

### PrivateGKEFirewallRules

This stage creates the appropriate firewall rules required for Pods to communicate to each other across multiple GKE clusters.

### HubService

This stage creates a [Anthos GKE Hub Service Account](https://cloud.google.com/anthos/multicluster-management/connect/prerequisites#gke-cross-project) that is used to register GKE clusters to a different project. In this architecture, you register the four clusters to an [environ host project](https://cloud.google.com/anthos/multicluster-management/environs#environ-host-project). You can register clusters from other projects to a single environ host project. In this architecture, you register all four GKE clusters (running in the `bank` and `shop` service projects) to the host network project. The host network project acts as the environ host project.

### ConfigureHub

This stage configures the Anthos GKE Hub service account IAM permissions so that the GKE clusters can register to the environ host project (or the host network project).

### RegisterGKE

This stage registers the four clusters to the Anthos GKE hub environ host project.

### ASMPrepProjects

This stage runs the [initialize script](https://cloud.google.com/service-mesh/docs/gke-multiproject-install-asm#setting_credentials_and_permissions) to prepare the projects for ASM installation. The script runs in the `bank` and the `shop` service projects.

### ASMGKEInstall

This stage installs [ASM](https://cloud.google.com/service-mesh/docs/gke-multiproject-install-asm#installing_anthos_service_mesh) on all four GKE clusters.

### ASMMulticlusterServiceDiscovery

This stage creates [kubeconfig secrets](https://cloud.google.com/service-mesh/docs/gke-install-multi-cluster#configure_endpoint_discovery_between_clusters) for all GKE clusters in all GKE clusters so that the ASM controlplane can automatically discover and update Services and Endpoints.

### ASMVerifyMulticluster

This stage deploys the [`whereami`](https://github.com/GoogleCloudPlatform/kubernetes-engine-samples/tree/master/whereami) and [`sleep`](https://github.com/istio/istio/tree/master/samples/sleep) apps in a `sample` namespace in all four clusters. The `sleep` app is a simple curl utility and the `whereami` app responds with metadata regarding where the service and Pods are running. The stage uses the `sleep` app to curl the `whereami` Pods running in all four clusters from all four clusters to ensure ASM multicluster functionality.

## Accessing clusters

1. Get access to the GKE clusters.

   ```bash
   export FOLDER_ID=$(kubectl --context=${KCC_GKE} -n ${ORG_SHORT_NAME} get folder -o=jsonpath='{.items[].status.folderId}')
   [ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" || echo "FOLDER_ID is $FOLDER_ID"
   echo -e "export FOLDER_ID=$FOLDER_ID" >> $WORKDIR/vars.sh
   gcloud config set project ${SVC_1_PROJECT}-${FOLDER_ID}

   gcloud container clusters get-credentials ${GKE1} --zone ${GKE1_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE2} --zone ${GKE2_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE3} --zone ${GKE3_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE4} --zone ${GKE4_ZONE} --project ${SVC_1_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE5} --zone ${GKE5_ZONE} --project ${SVC_2_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE6} --zone ${GKE6_ZONE} --project ${SVC_2_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE7} --zone ${GKE7_ZONE} --project ${SVC_2_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE8} --zone ${GKE8_ZONE} --project ${SVC_2_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE9} --zone ${GKE9_ZONE} --project ${SVC_3_PROJECT}-${FOLDER_ID}
   gcloud container clusters get-credentials ${GKE10} --zone ${GKE10_ZONE} --project ${SVC_3_PROJECT}-${FOLDER_ID}

   kubectl ctx ${GKE1}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE1_ZONE}_${GKE1}
   kubectl ctx ${GKE2}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE2_ZONE}_${GKE2}
   kubectl ctx ${GKE3}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE3_ZONE}_${GKE3}
   kubectl ctx ${GKE4}=gke_${SVC_1_PROJECT}-${FOLDER_ID}_${GKE4_ZONE}_${GKE4}
   kubectl ctx ${GKE5}=gke_${SVC_2_PROJECT}-${FOLDER_ID}_${GKE5_ZONE}_${GKE5}
   kubectl ctx ${GKE6}=gke_${SVC_2_PROJECT}-${FOLDER_ID}_${GKE6_ZONE}_${GKE6}
   kubectl ctx ${GKE7}=gke_${SVC_2_PROJECT}-${FOLDER_ID}_${GKE7_ZONE}_${GKE7}
   kubectl ctx ${GKE8}=gke_${SVC_2_PROJECT}-${FOLDER_ID}_${GKE8_ZONE}_${GKE8}
   kubectl ctx ${GKE9}=gke_${SVC_3_PROJECT}-${FOLDER_ID}_${GKE9_ZONE}_${GKE9}
   kubectl ctx ${GKE10}=gke_${SVC_3_PROJECT}-${FOLDER_ID}_${GKE10_ZONE}_${GKE10}
   ```

**Note** - If your terminal's public IP address changes, you will lose access to the GKE clusters. You will need to update the [`authorized networks`](https://cloud.google.com/kubernetes-engine/docs/how-to/authorized-networks) on all clusters with your terminal's new public IP. You can get your terminal's public IP from `curl ifconfig.me`. Or you can run the following script `$WORKDIR/asm-shared-vpc/scripts/update-clusters-auth-networks.sh`.

## Deploying Bank of Anthos

In this section, you deploy Bank of Anthos to GKE1 and GKE2 clusters in the `bank` service project using Gitlab project and Gitlab CI.

1.  Create a new Gitlab project for the Bank of Anthos application.

    ```bash
    cd $WORKDIR
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=${GITLAB_BANK_PROJECT_NAME}"
    ```

1.  Get the Gitlab project ID of the project you just created.

    ```bash
    export GITLAB_BANK_PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_BANK_PROJECT_NAME "$GITLAB_BANK_PROJECT_NAME" '.[] | select(.name==$GITLAB_BANK_PROJECT_NAME)' | jq -r '.id')
    [ -z "$GITLAB_BANK_PROJECT_ID" ] && echo "GITLAB_BANK_PROJECT_ID is not exported" || echo "GITLAB_BANK_PROJECT_ID is $GITLAB_BANK_PROJECT_ID"
    echo -e "export GITLAB_BANK_PROJECT_ID=$GITLAB_BANK_PROJECT_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_BANK_PROJECT_ID is 242937234

1.  Get Gitlab runner ID running in the `kcc` cluster. You can unlock this runner and use the same runner in multiple Gitlab projects.

    ```bash
    export GITLAB_RUNNER_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/runners?status=active" | jq '.[] | select(.is_shared==false)' | jq -r '.id')
    [ -z "$GITLAB_RUNNER_ID" ] && echo "GITLAB_RUNNER_ID is not exported" || echo "GITLAB_RUNNER_ID is $GITLAB_RUNNER_ID"
    echo -e "export GITLAB_RUNNER_ID=$GITLAB_RUNNER_ID" >> $WORKDIR/vars.sh
    ```

1.  Unlock the runner from the ASM project so that you can use the same runner in the `bank` and later in the `shop` projects.

    ```bash
    curl -s --request PUT --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/runners/${GITLAB_RUNNER_ID}" --form "locked=false"
    ```

1.  Enable the runner in the `bank` project.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/runners" --form "runner_id=${GITLAB_RUNNER_ID}"
    ```

1.  Disable shared runners on the project.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X PUT "https://gitlab.com/api/v4/projects/$GITLAB_BANK_PROJECT_ID" --form "shared_runners_enabled=false"
    ```

1.  Verify that the runner is enabled in the `bank` project.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_BANK_PROJECT_ID/runners?status=active" | jq '.[] | select(.is_shared==false)'
    ```

The output is similar to the following:

    {
      "id": 123456,
      "description": "gitlab-runner-gitlab-runner-123456abcde-pjrqc",
      "ip_address": "104.104.140.140",
      "active": true,
      "is_shared": false,
      "name": "gitlab-runner",
      "online": true,
      "status": "online"
    }

1.  Create variables in your Gitlab project which are used by the Gitlab CI pipeline.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA_KEY" --form "value=$(cat ${GITLAB_CI_GCP_SA_NAME}-key-base64.txt)" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=BILLING_ACCOUNT" --form "value=$BILLING_ACCOUNT" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA" --form "value=${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=INFRA_ADMIN_PROJECT_ID" --form "value=${KCC_PROJECT}-${KCC_FOLDER_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=ORG_ID" --form "value=${ORG_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=ORG_NAME" --form "value=${ORG_SHORT_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_BANK_PROJECT_ID}/variables" --form "key=FOLDER_NAME" --form "value=${FOLDER_NAME}" --form "protected=true"
    ```

1.  Verify the variables are properly configured.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/projects/"${GITLAB_BANK_PROJECT_ID}"/variables | jq
    ```

1.  Get your Gitlab project's SSH URL.

    ```bash
    export GITLAB_BANK_PROJECT_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_BANK_PROJECT_NAME "$GITLAB_BANK_PROJECT_NAME" '.[] | select(.name==$GITLAB_BANK_PROJECT_NAME)' | jq -r '.ssh_url_to_repo')
    [ -z "$GITLAB_BANK_PROJECT_SSH_URL" ] && echo "GITLAB_BANK_PROJECT_SSH_URL is not exported" || echo "GITLAB_BANK_PROJECT_SSH_URL is $GITLAB_BANK_PROJECT_SSH_URL"
    echo -e "export GITLAB_BANK_PROJECT_SSH_URL=$GITLAB_BANK_PROJECT_SSH_URL" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_BANK_PROJECT_SSH_URL is git@gitlab.com:ameer00/ameerab-asm-bank-of-anthos.git

1.  Clone the empty repo using SSH.

    ```bash
    cd ${WORKDIR}
    git clone $GITLAB_BANK_PROJECT_SSH_URL ${ORG_SHORT_NAME}-asm-bank-of-anthos
    ```

The output is similar to the following:

    Cloning into 'org-asm-bank-of-anthos'...
    warning: You appear to have cloned an empty repository.

1.  Clone the ASM repo and copy the contents of the ASM repo into your empty repo.

    ```bash
    cp -r asm-shared-vpc/bank-of-anthos/. ${ORG_SHORT_NAME}-asm-bank-of-anthos
    ```

1.  Commit the changes to your repo.

    ```bash
    cd ${ORG_SHORT_NAME}-asm-bank-of-anthos
    git add .
    git commit -m "initial commit"
    git push
    ```

1.  Access the following link to inspect the Gitlab pipeline.

    ```bash
    export GITLAB_BANK_PROJECT_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_BANK_PROJECT_NAME "$GITLAB_BANK_PROJECT_NAME" '.[] | select(.name==$GITLAB_BANK_PROJECT_NAME)' | jq -r '.web_url')
    echo -e "export GITLAB_BANK_PROJECT_WEB_URL=$GITLAB_BANK_PROJECT_WEB_URL" >> $WORKDIR/vars.sh
    echo -n "${GITLAB_BANK_PROJECT_WEB_URL}"/-/pipelines
    ```

<img src="/docs/bank-pipeline.png" width=90% height=90%>

You can click on individual stages and monitor progress of all the steps within each stage.  
This pipeline takes about 5 - 10 minutes to complete.  
This simple pipeline consists of three stages. The first stage deploys the application manifests to the clusters. The second stage verifies that the deployments are available and all Pods are running. The last stage outputs the `istio-ingressgateway` public IP address where the `frontend` Service is exposed. You can grab either IP addresses and access the Bank of Anthos application.

1.  Click on the last job called `get-frontend-ip-address` in the third stage (Access) and get the `istio-ingressgateway` public IP address from either of the two GKE clusters to access the Bank of Anthos application.

The output is similar to the following:

    GKE1 istio-ingressgateway public IP address is 35.235.83.250
    GKE2 istio-ingressgateway public IP address is 34.94.247.89

Access the application, login, create a deposit and transfer funds to other accounts. You can also create a new user and login using the new user and perform transactions.  
All features/services of the applications should be functional.

## Deploying Online Boutique

In this section, you deploy Online Boutique to GKE3 and GKE4 clusters in the `shop` project using Gitlab project and Gitlab CI.

1.  Create a new Gitlab project for the Online Boutique application.

    ```bash
    cd $WORKDIR
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=${GITLAB_SHOP_PROJECT_NAME}"
    ```

1.  Get the Gitlab project ID of the project you just created.

    ```bash
    export GITLAB_SHOP_PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_SHOP_PROJECT_NAME "$GITLAB_SHOP_PROJECT_NAME" '.[] | select(.name==$GITLAB_SHOP_PROJECT_NAME)' | jq -r '.id')
    [ -z "$GITLAB_SHOP_PROJECT_ID" ] && echo "GITLAB_SHOP_PROJECT_ID is not exported" || echo "GITLAB_SHOP_PROJECT_ID is $GITLAB_SHOP_PROJECT_ID"
    echo -e "export GITLAB_SHOP_PROJECT_ID=$GITLAB_SHOP_PROJECT_ID" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_SHOP_PROJECT_ID is 242937234

1.  Enable the runner in the `shop` project.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/runners" --form "runner_id=${GITLAB_RUNNER_ID}"
    ```

1.  Disable shared runners on the project.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X PUT "https://gitlab.com/api/v4/projects/$GITLAB_SHOP_PROJECT_ID" --form "shared_runners_enabled=false"
    ```

1.  Verify that the runner is enabled in the `bank` project.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$GITLAB_SHOP_PROJECT_ID/runners?status=active" | jq '.[] | select(.is_shared==false)'
    ```

The output is similar to the following:

    {
      "id": 123456,
      "description": "gitlab-runner-gitlab-runner-123456abcde-pjrqc",
      "ip_address": "104.104.140.140",
      "active": true,
      "is_shared": false,
      "name": "gitlab-runner",
      "online": true,
      "status": "online"
    }

1.  Create variables in your Gitlab project which are used by the Gitlab CI pipeline.

    ```bash
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA_KEY" --form "value=$(cat ${GITLAB_CI_GCP_SA_NAME}-key-base64.txt)" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=BILLING_ACCOUNT" --form "value=$BILLING_ACCOUNT" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=GITLAB_GCP_SA" --form "value=${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=INFRA_ADMIN_PROJECT_ID" --form "value=${KCC_PROJECT}-${KCC_FOLDER_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=ORG_ID" --form "value=${ORG_ID}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=ORG_NAME" --form "value=${ORG_SHORT_NAME}" --form "protected=true"
    curl -s --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${GITLAB_SHOP_PROJECT_ID}/variables" --form "key=FOLDER_NAME" --form "value=${FOLDER_NAME}" --form "protected=true"
    ```

1.  Verify the variables are properly configured.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/projects/"${GITLAB_SHOP_PROJECT_ID}"/variables | jq
    ```

1.  Get your Gitlab project's SSH URL.

    ```bash
    export GITLAB_SHOP_PROJECT_SSH_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_SHOP_PROJECT_NAME "$GITLAB_SHOP_PROJECT_NAME" '.[] | select(.name==$GITLAB_SHOP_PROJECT_NAME)' | jq -r '.ssh_url_to_repo')
    [ -z "$GITLAB_SHOP_PROJECT_SSH_URL" ] && echo "GITLAB_SHOP_PROJECT_SSH_URL is not exported" || echo "GITLAB_SHOP_PROJECT_SSH_URL is $GITLAB_SHOP_PROJECT_SSH_URL"
    echo -e "export GITLAB_SHOP_PROJECT_SSH_URL=$GITLAB_SHOP_PROJECT_SSH_URL" >> $WORKDIR/vars.sh
    ```

The output is similar to the following:

    GITLAB_SHOP_PROJECT_SSH_URL is git@gitlab.com:ameer00/ameerab-asm-online-boutique.git

1.  Clone the empty repo using SSH.

    ```bash
    cd ${WORKDIR}
    git clone $GITLAB_SHOP_PROJECT_SSH_URL ${ORG_SHORT_NAME}-asm-online-boutique
    ```

The output is similar to the following:

    Cloning into 'org-asm-online-boutique'...
    warning: You appear to have cloned an empty repository.

1.  Clone the ASM repo and copy the contents of the ASM repo into your empty repo.

    ```bash
    cp -r asm-shared-vpc/online-boutique/. ${ORG_SHORT_NAME}-asm-online-boutique
    ```

1.  Commit the changes to your repo.

    ```bash
    cd ${ORG_SHORT_NAME}-asm-online-boutique
    git add .
    git commit -m "initial commit"
    git push
    ```

1.  Access the following link to inspect the Gitlab pipeline.

    ```bash
    export GITLAB_SHOP_PROJECT_WEB_URL=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/projects | jq --arg GITLAB_SHOP_PROJECT_NAME "$GITLAB_SHOP_PROJECT_NAME" '.[] | select(.name==$GITLAB_SHOP_PROJECT_NAME)' | jq -r '.web_url')
    echo -e "export GITLAB_SHOP_PROJECT_WEB_URL=$GITLAB_SHOP_PROJECT_WEB_URL" >> $WORKDIR/vars.sh
    echo -n "${GITLAB_SHOP_PROJECT_WEB_URL}"/-/pipelines
    ```

    <img src="/docs/shop-pipeline.png" width=90% height=90%>

You can click on individual stages and monitor progress of all the steps within each stage.  
This pipeline takes about 5 - 10 minutes to complete.  
This simple pipeline consists of three stages. The first stage deploys the application manifests to the clusters. The second stage verifies that the deployments are available and all Pods are running. The last stage outputs the `istio-ingressgateway` public IP address where the `frontend` Service is exposed. You can grab either IP addresses and access the Online Boutique application.

1.  Click on the last job called `get-frontend-ip-address` in the third stage (Access) and get the `istio-ingressgateway` public IP address from either of the two GKE clusters to access the Online Boutique application.

The output is similar to the following:

    GKE3 istio-ingressgateway public IP address is 34.121.7.75
    GKE4 istio-ingressgateway public IP address is 34.66.210.16

Access the application, browse through items, add them to your cart and checkout.  
All features/services of the applications should be functional.

## Cleaning up

To avoid incurring charges to your Google Cloud Platform account for the resources used in this tutorial:  
You can delete all the resources, the four projects and the folder you created for this guide. You can also delete the KCC folder and the `proj-infra-admin` project.  
When you create a shared VPC, a [lien](https://cloud.google.com/resource-manager/docs/project-liens) is placed on the network host project. The lien must be removed prior to deleting the network host project. The two service projects can be deleted.

1.  Delete the network lien (for the shared VPC) and the four projects. You can undelete these projects within 30 days of deletion.

    ```bash
    source $WORKDIR/vars.sh
    export FOLDER_ID=$(kubectl --context=${KCC_GKE} -n ${ORG_SHORT_NAME} get folder -o=jsonpath='{.items[].status.folderId}')
    [ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" || echo "FOLDER_ID is $FOLDER_ID"
    export LIEN_ID=$(gcloud alpha resource-manager liens list --project="${HOST_NET_PROJECT}-${FOLDER_ID}" | awk 'NR==2 {print $1}')
    gcloud alpha resource-manager liens delete $LIEN_ID
    gcloud projects delete "${HOST_NET_PROJECT}-${FOLDER_ID}" --quiet
    gcloud projects delete "${SVC_1_PROJECT}-${FOLDER_ID}" --quiet
    gcloud projects delete "${SVC_2_PROJECT}-${FOLDER_ID}" --quiet
    gcloud projects delete "${SVC_3_PROJECT}-${FOLDER_ID}" --quiet
    ```

The output is similar to the following:

    Deleted [https://cloudresourcemanager.googleapis.com/v1/projects/proj-1-bank-prod-814581352178].

1.  Delete the folder

    ```bash
    gcloud resource-manager folders delete ${FOLDER_ID}
    ```

The output is similar to the following:

    Deleted [<Folder
     createTime: '2021-01-29T21:58:38.290Z'
     displayName: 'asm18-shared-vpc'
     lifecycleState: LifecycleStateValueValuesEnum(DELETE_REQUESTED, 2)
     name: 'folders/814581352178'
     parent: 'organizations/542306964273'>].

1.  Delete the `proj-infra-admin` project, and the KCC and main folders.

    ```bash
    gcloud projects delete "${KCC_PROJECT}-${KCC_FOLDER_ID}" --quiet
    gcloud resource-manager folders delete ${KCC_FOLDER_ID}
    gcloud resource-manager folders delete ${MAIN_FOLDER_ID}
    ```

1.  Unset `KUBECONFIG`.

    ```bash
    unset KUBECONFIG
    ```

1.  Delete the Gitlab projects and group.

    ```bash
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/projects/$GITLAB_INFRA_PROJECT_ID"
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/projects/$GITLAB_CONFIG_PROJECT_ID"
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/projects/$GITLAB_BANK_PROJECT_ID"
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/projects/$GITLAB_SHOP_PROJECT_ID"
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/groups/$GITLAB_GROUP_ID"
    ```

The output is similar to the following:

    {"message":"202 Accepted"}

1.  Delete the Gitlab SSH key.

    ```bash
    export GITLAB_SSH_KEY_ID=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" https://gitlab.com/api/v4/users/"${GITLAB_USER_ID}"/keys | jq --arg GITLAB_SSH_KEY "$GITLAB_SSH_KEY" '.[] | select(.title==$GITLAB_SSH_KEY)' | jq -r '.id')
    curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X DELETE "https://gitlab.com/api/v4/user/keys/$GITLAB_SSH_KEY_ID"
    ```

1.  Remove KCC SA as a billing user from your billing account.

    ```bash
    gcloud beta billing accounts get-iam-policy ${BILLING_ACCOUNT} --format=json | \
    jq '(.bindings[] | select(.role=="roles/billing.user").members) -= ["serviceAccount:'${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com'"]' > $WORKDIR/billing-iam-policy.json
    gcloud beta billing accounts set-iam-policy ${BILLING_ACCOUNT} $WORKDIR/billing-iam-policy.json
    ```

1.  Remove the KCC SA and Gitlab CI SA Organization IAM roles.

    ```bash
    kcc_sa_iam_roles=(
        roles/resourcemanager.organizationAdmin
        roles/billing.admin
        roles/resourcemanager.folderAdmin
        roles/resourcemanager.projectCreator
        roles/compute.admin
    )
    for role in "${kcc_sa_iam_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
        --member=serviceAccount:${KCC_SERVICE_ACCOUNT}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role="${role}"
    done

    gitlab_sa_iam_roles=(
        roles/gkehub.admin
        roles/compute.admin
        roles/container.admin
        roles/owner
    )
    for role in "${gitlab_sa_iam_roles[@]}"
    do
        gcloud --project=${KCC_PROJECT}-${KCC_FOLDER_ID} organizations remove-iam-policy-binding ${ORG_ID} \
        --member=serviceAccount:${GITLAB_CI_GCP_SA_NAME}@${KCC_PROJECT}-${KCC_FOLDER_ID}.iam.gserviceaccount.com \
        --role="${role}"
    done
    ```

1.  Delete `WORKDIR`.

    ```bash
    cd $HOME
    rm -rf $WORKDIR
    ```

1.  Optionally, you can also revoke the API personal access token by navigating to the following page.

    ```bash
    echo -n "https://gitlab.com/-/profile/personal_access_tokens"
    ```

## [ASM Shared VPC with Multiple Service Projects: The Hard Way](/docs/manual.md)

Click on the link if you want to build the same architecture step by step using command line.

## Appendix

Documentation used for this guide.

- https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-shared-vpc
- https://cloud.google.com/anthos/multicluster-management/connect/prerequisites
- https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity
- https://cloud.google.com/anthos/multicluster-management/environs#environ-host-project
- https://cloud.google.com/service-mesh/docs/gke-multiproject-install-asm
- https://cloud.google.com/kubernetes-engine/docs/how-to/troubleshooting-and-ops#avmbr110_iam_permission_permission_missing_for_cluster_name
